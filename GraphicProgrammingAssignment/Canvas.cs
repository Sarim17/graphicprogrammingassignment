﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicProgrammingAssignment
{
    public class Canvas
    {


        Graphics g;
        Pen Pen;
        SolidBrush solidBrush;
        int xPos, yPos;

        public Canvas(Graphics g)
        {
            this.g = g;

            xPos = 0;
            yPos = 0;
            Pen = new Pen(Color.Black, 1);
            solidBrush = new SolidBrush(Color.Black);


        }

        public void DrawLine(int toX, int toY)
        {
            g.DrawLine(Pen, xPos, yPos, xPos + toX, yPos + toY);
            xPos += toX;
            yPos += toY;

        }

        public void drawTo(int toX, int toY)
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY);
            xPos = toX;
            yPos = toY;
        }

        public void moveTo(int toX, int toY)
        {
            xPos = toX;
            yPos = toY;
        }


        public void rect(int sizeX, int sizeY, bool fill)
        {
            if (fill)
            {
                g.FillRectangle(solidBrush, xPos, yPos, sizeX, sizeY);
            }
            else
            {
                g.DrawRectangle(Pen, xPos, yPos, sizeX, sizeY);
            }
                
            //g.FillRectangle();
        }

        public void circle(int radius, bool fill)
        {
            int d = radius / 2;
            Rectangle r = new Rectangle(xPos - d, yPos - d, radius, radius);

            if (fill)
            {
                g.FillEllipse(solidBrush, r);
            }
            else
            {
                g.DrawEllipse(Pen, r);
            }

        }

        public void triangle(int size, bool fill)
        {
            Point[] points = { new Point(xPos, yPos), new Point(xPos + size, yPos), new Point(xPos + (size/2), yPos - size) };


            if (fill)
            {
                g.FillPolygon(solidBrush, points);
            }
            else
            {
                g.DrawPolygon(Pen, points);
            }

        }


        public void reset()
        {
            xPos = 0;
            yPos = 0;
        }


        public void clear()
        {
            g.Clear(Color.Empty);
        }

        public void loadBitmap(Image img)
        {
            g.DrawImage(img, 200,200);
        }


        public void penColor(String color)
        {
            //Pen.Color = ColorTranslator.FromHtml("#ACFEFF");
            Pen.Color = Color.FromName(color);
            solidBrush.Color = Color.FromName(color);

        }

        public int[] getXY()
        {
            int[] intArrayPos = new int[] { xPos, yPos };
            return intArrayPos;
        }

        public String getPenColour()
        {
            

            string name = ColorTranslator.ToHtml(Pen.Color) + "/ Unknown Name" ;
            foreach (KnownColor kc in Enum.GetValues(typeof(KnownColor)))
            {
                Color known = Color.FromKnownColor(kc);
                if (Pen.Color.ToArgb() == known.ToArgb())
                {
                    name = known.Name;
                }
            }

            return name;
        }

    }
}
