﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicProgrammingAssignment
{
    public partial class mainForm : Form
    {
        Bitmap myBitmap = new Bitmap(640, 480);
        Canvas DrawingCanvas;

        bool fillShapes = false;

        public mainForm()
        {
            InitializeComponent();
            DrawingCanvas = new Canvas(Graphics.FromImage(myBitmap));

            UpdateToolStatus();

        }

        private void cmdLine_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                String cmdInputText = cmdLine.Text.Trim().ToLower();

                Console.WriteLine(cmdInputText);

                if (cmdInputText.Equals("run"))
                {
                    cmdInputText = programTextBox.Text.Trim().ToLower();
                }
                else
                {
                    cmdInputText = cmdLine.Text.Trim().ToLower();
                }

                runCommand(cmdInputText);
                cmdLine.Text = "";

                


            }
        }


        private void canvasWindow_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(myBitmap, 0,0);
        }


        private void UpdateToolStatus()
        {

            String xPosText = DrawingCanvas.getXY()[0].ToString();
            String yPosText = DrawingCanvas.getXY()[1].ToString();
            String penColour = "Pen " + DrawingCanvas.getPenColour();
            
            String fillStatus;

            if (fillShapes) { fillStatus = "On"; } else { fillStatus = "Off"; }

            toolStripStatusLabel.Text = penColour + " - Fill: " + fillStatus + " - Current Pen Position: " + xPosText + ", " + yPosText;

        }

        private void runScriptButton_Click(object sender, EventArgs e)
        {
            String cmdInputText = programTextBox.Text.Trim().ToLower();
            Console.WriteLine(cmdInputText);

            

            runCommand(cmdInputText);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "PNG Image|*.png" +
                "|All Files|*.*";
            dialog.FileName = "canvas";
            dialog.DefaultExt = "png";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                myBitmap.Save(dialog.FileName);
            }
           
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog openFileDialog1 = new OpenFileDialog()
            {
                FileName = "Select a image file",
                Filter = "PNG Image|*.png",
                Title = "Open image file"
            };

            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Console.WriteLine(openFileDialog1.FileName);
                Bitmap myBitmap2 = new Bitmap(openFileDialog1.FileName);
                //canvasWindow.Image = myBitmap2;
                DrawingCanvas.loadBitmap(myBitmap2);
                Refresh();

            }

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DrawingCanvas.clear();
            DrawingCanvas.reset();
            programTextBox.Text = "";
            cmdLine.Text = "";
            cmdHistory.Text = "";
        }

        public void runCommand(String cmdInputText)
        {

            string[] lines = cmdInputText.Split(
            new[] { "\r\n", "\r", "\n" },
            StringSplitOptions.None
            );

            foreach (String s in lines)
            {

            String[] cmdParameters = s.Split();

            switch (cmdParameters[0])
            {
                case "drawto":

                    try
                    {
                        String[] pos = cmdParameters[1].Split(',');

                        int x, y;
                        bool firstPara = Int32.TryParse(pos[0], out x);
                        bool secondPara = Int32.TryParse(pos[1], out y);

                        if (firstPara && secondPara)
                        {
                            cmdHistory.AppendText(s + Environment.NewLine);
                            DrawingCanvas.drawTo(x, y);
                        }
                        else
                        {
                            cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                    }

                    break;

                case "moveto":

                    try
                    {
                        String[] pos = cmdParameters[1].Split(',');

                        int x, y;
                        bool firstPara = Int32.TryParse(pos[0], out x);
                        bool secondPara = Int32.TryParse(pos[1], out y);

                        if (firstPara && secondPara)
                        {
                            cmdHistory.AppendText(s + Environment.NewLine);
                            DrawingCanvas.moveTo(x, y);
                        }
                        else
                        {
                            cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                    }

                    break;
                case "rect":

                    try
                    {
                        String[] pos = cmdParameters[1].Split(',');

                        int x, y;
                        bool firstPara = Int32.TryParse(pos[0], out x);
                        bool secondPara = Int32.TryParse(pos[1], out y);

                        if (firstPara && secondPara)
                        {
                            cmdHistory.AppendText(s + Environment.NewLine);
                            DrawingCanvas.rect(x, y, fillShapes);
                        }
                        else
                        {
                            cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                    }

                    break;
                case "circle":

                    try
                    {
                        int r;
                        bool firstPara = Int32.TryParse(cmdParameters[1], out r);

                        if (firstPara)
                        {
                            cmdHistory.AppendText(s + Environment.NewLine);
                            DrawingCanvas.circle(r, fillShapes);
                        }
                        else
                        {
                            cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                    }
                    break;
                case "triangle":

                        try
                        {
                            int size;
                            bool firstPara = Int32.TryParse(cmdParameters[1], out size);

                            if (firstPara)
                            {
                                cmdHistory.AppendText(s + Environment.NewLine);
                                DrawingCanvas.triangle(size, fillShapes);
                            }
                            else
                            {
                                cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                        }

                        break;
                case "color":

                    if (cmdParameters[1] == "red" || cmdParameters[1] == "green" || cmdParameters[1] == "blue" || cmdParameters[1] == "yellow")
                    {
                        DrawingCanvas.penColor(cmdParameters[1]);
                        cmdHistory.AppendText(s + Environment.NewLine);
                    }
                    else
                    {
                        cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                    }

                    break;
                case "fill":

                        try
                        {
                            if (cmdParameters[1] == "on")
                            {
                                fillShapes = true;
                                cmdHistory.AppendText(s + Environment.NewLine);
                            }
                            else if (cmdParameters[1] == "off")
                            {
                                fillShapes = false;
                                cmdHistory.AppendText(s + Environment.NewLine);
                            }
                        }
                        catch (IndexOutOfRangeException)
                        {
                            cmdHistory.AppendText("Invalid Parameters" + Environment.NewLine);
                        }

                    break;
                case "reset":
                    DrawingCanvas.reset();
                    cmdHistory.AppendText(s + Environment.NewLine);
                    break;
                case "clear":
                    DrawingCanvas.clear();
                    cmdHistory.AppendText(s + Environment.NewLine);
                    break;
                case "":
                        cmdHistory.AppendText("No Command Written" + Environment.NewLine);

                        break;


                    default:
                    Console.WriteLine("invalid commmand");
                    cmdHistory.AppendText("Invalid Command" + Environment.NewLine);
                    break;
                }

                Refresh();
                UpdateToolStatus();
            }
        }
    }
}
