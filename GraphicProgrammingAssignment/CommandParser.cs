﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicProgrammingAssignment
{
    class CommandParser
    {

        public void parseCmdLine(String cmdLineText, Canvas canvas)
        {
            cmdLineText = cmdLineText.Trim().ToLower();
            String[] cmdParameters = cmdLineText.Split();

            switch (cmdParameters[0])
            {
                case "drawto":
                    break;
                case "moveto":
                    break;
                case "rect":
                    break;
                case "circle":
                    break;
                case "triangle":
                    break;
                case "color":
                    break;
                case "fill":
                    break;
                case "reset":
                    break;
                case "clear":
                    break;
                default:
                    break;
            }
        }
    }
}
